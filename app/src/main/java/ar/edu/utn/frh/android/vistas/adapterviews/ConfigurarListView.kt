package ar.edu.utn.frh.android.vistas.adapterviews

import android.content.Context
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView

/**
 * Funcion para configurar el ListView de este ejemplo.
 * Los datos están harcodeados y los Strings no están en archivos de recursos XML
 * porque esto es un ejemplo
 */
fun configurarListView(context: Context, view: View) {
    val items = listOf(
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
    )

    // Usamos ArrayAdapter porque sólo queremos mostrar un String
    // Usamos recursos que ya vienen con Android, por eso android.R en lugar de R
    // Estos recursos no se pueden modificar, pero siempre podemos crear los nuestros propios
    // (para eso ver ejemplo de Spinner)
    val adapter = ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, android.R.id.text1)
    val lv = view as ListView
    lv.adapter =  adapter
    adapter.addAll(items)
}