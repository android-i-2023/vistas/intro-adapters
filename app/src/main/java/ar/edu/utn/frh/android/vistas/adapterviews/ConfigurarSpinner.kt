package ar.edu.utn.frh.android.vistas.adapterviews

import android.content.Context
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner

/**
 * Funcion para configurar el Spinner de este ejemplo.
 * Los datos están harcodeados y los Strings no están en archivos de recursos XML
 * porque esto es un ejemplo
 */

fun configurarSpinner(context: Context, view: View) {
    val items = listOf(
        "Lunes",
        "Martes",
        "Miércoles",
        "Jueves",
        "Viernes",
    )

    // Usamos ArrayAdapter porque sólo queremos mostrar un String
    // A diferencia del ejemplo de ListView, aquí usamos nuestros propios recursos
    // para tener más control de cómo se muestran las cosas.
    val adapter = ArrayAdapter<String>(context, R.layout.item_spinner, android.R.id.text1)
    val spinner = view.findViewById<Spinner>(R.id.spinner)
    spinner.adapter = adapter
    adapter.addAll(items)
}