package ar.edu.utn.frh.android.vistas.adapterviews

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import ar.edu.utn.frh.android.vistas.adapterviews.util.SimplePageChangeListener

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Los datos de nuestro PagerAdapter van a ser simplemente los layout IDs para inflar
        // En caso real esto deberían ser objetos más complejos
        val paginas = listOf(
            R.layout.ejemplo_listview,
            R.layout.ejemplo_gridview,
            R.layout.ejemplo_spinner,
            R.layout.ejemplo_recyclerview,
        )

        val titulosPaginas = listOf(
            "Ejemplo - ListView",
            "Ejemplo - Spinner",
            "Ejemplo - GridView",
            "Ejemplo - RecyclerView",
        )

        val pager: ViewPager = findViewById(R.id.pager)
        pager.adapter = EjemploAdapter(this, paginas, titulosPaginas)

        // Capturamos el evento de que el usuario cambie de página para cambiar el título de la Activity
        pager.addOnPageChangeListener(object: SimplePageChangeListener() {
            override fun onPageSelected(position: Int) {
                // Usamos !! porque estamos seguros de que pager.adapter está asignado
                title = pager.adapter!!.getPageTitle(position)
            }
        })
    }
}
