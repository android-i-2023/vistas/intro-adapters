package ar.edu.utn.frh.android.vistas.adapterviews.util

import androidx.viewpager.widget.ViewPager

/**
 * Clase de conveniencia para que al momento de crear un listener de esta clase
 * alcance con redefinir un solo método en lugar de los tres,
 * mejorando la legibilidad del código.
 *
 * Esto no es obligatorio (o si no Android ya tendría esta clase),
 * sólo es algo que a varios devs nos gusta hacer :)
 */
open class SimplePageChangeListener: ViewPager.OnPageChangeListener {
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }

    override fun onPageScrollStateChanged(state: Int) {
    }
}