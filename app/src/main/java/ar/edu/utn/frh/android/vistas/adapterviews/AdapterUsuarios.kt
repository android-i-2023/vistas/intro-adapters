package ar.edu.utn.frh.android.vistas.adapterviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import ar.edu.utn.frh.android.vistas.adapterviews.modelo.Usuario

/**
 * Array que a partir de una lista de usuario es capaz de generar vistas que los representan.
 *
 * Como hereda de ArrayAdapter, posee internamente un Array para guardar los usuarios,
 * por eso no es necesario indicarle cuántos elementos posee.
 *
 * El método getView() de ArrayAdapter puede inflar un layout para generar las vistas de este Adapter,
 * para eso hay que darle el ID del layout.
 *
 * También puede representar el dato de la vista en forma de String y ubicarlo en un TextView de la vista,
 * para eso hay que darle el ID del TextView.
 *
 * En nuestro caso necesitamos llenar dos TextViews, no uno, de manera que el comportamiento normal de ArrayAdapter
 * en este caso no nos sirve, y por eso vamos a redefinir getView()
 */
class AdapterUsuarios(context: Context): ArrayAdapter<Usuario>(context, R.layout.item_gridview) {
    // LayoutInflater genera Views de Android a partir de archivos XML de layout
    private val inflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Si ya se habia creado una View para esta posicion, la recibimos en convertView para reutilizarla.
        // Esto es asi porque inflar vistas es un proceso costoso y si es posible hay que evitarlo.
        // Si es la primera vez que generamos la View, convertView va a ser null y va a haber que inflar
        val view = convertView ?: inflater.inflate(R.layout.item_gridview, parent, false)

        // Buscamos los TextViews que vamos a llenar
        val tvTitulo = view.findViewById<TextView>(R.id.titulo)
        val tvFecha = view.findViewById<TextView>(R.id.fecha)

        // Llenamos los TextViews con los datos que corresponda
        val item = getItem(position)!!
        tvTitulo.text = item.nombre
        tvFecha.text = item.usuarioDesde

        // Aca si o si devolver un View, no es como PagerAdapter.instantiateItem donde se puede devolver cualquier cosa
        return view
    }
}