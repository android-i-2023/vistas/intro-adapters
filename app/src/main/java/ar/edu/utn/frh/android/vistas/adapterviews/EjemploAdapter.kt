package ar.edu.utn.frh.android.vistas.adapterviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

/**
 * Adapter para ViewPager, como otros Adapters genera y devuelve vistas a partir de datos puros.
 *
 * PagerAdapter no tiene internamente un Array o un List para guardar los datos,
 * entonces lo proveemos por constructor, y le indicamos cuántas páginas tiene.
 */
class EjemploAdapter(
    private val context: Context,
    private val paginas: List<Int>,
    private val titulosPaginas: List<String>
): PagerAdapter() {

    override fun getCount() = paginas.size

    /** Crea la vista que necesita el ViewPager en determinada posición, y la agrega al ViewPager.
     *  Retorna cualquier objeto que se pueda asociar con esta vista, no necesita ser una vista como en ArrayAdapter
     */
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val pagina = paginas[position]

        // Creamos la vista
        val view = LayoutInflater.from(context).inflate(pagina, container, false)

        // Agregamos la vista al ViewPager
        val pager = container as ViewPager
        pager.addView(view)

        // Configuramos la vista de la página
        when (pagina) {
            R.layout.ejemplo_spinner -> configurarSpinner(context, view)
            R.layout.ejemplo_listview -> configurarListView(context, view)
            R.layout.ejemplo_gridview -> configurarGridView(context, view)
            R.layout.ejemplo_recyclerview -> {
                // próximamente
            }
        }

        // Lo que retornemos aquí será el segundo argumento de isViewFromObject cuando se invoque
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        // En este ejemplo no hace falta destruir nada
    }

    // Obtenemos el titulo de cada página
    override fun getPageTitle(position: Int): CharSequence {
        return titulosPaginas[position]
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }
}
