package ar.edu.utn.frh.android.vistas.adapterviews

import android.content.Context
import android.view.View
import android.widget.GridView
import ar.edu.utn.frh.android.vistas.adapterviews.modelo.Usuario

/**
 * Funcion para configurar el GridView de este ejemplo.
 * Los datos están harcodeados y los Strings no están en archivos de recursos XML
 * porque esto es un ejemplo
 */
fun configurarGridView(context: Context, view: View) {
    val adapter = AdapterUsuarios(context)
    val gv = view as GridView
    gv.adapter =  adapter
    adapter.addAll(
        Usuario("Jonathan Sanz", "1999"),
        Usuario("Adela Fernández", "2020"),
        Usuario("Matilde Marín", "2019"),
        Usuario("Natalia Calvo", "2022"),
        Usuario("Lourdes Castro", "2010"),
        Usuario("Antonia Muñoz", "2021"),
        Usuario("Valentín Durán", "2003"),
        Usuario("Victorino Blanco", "2007"),
        Usuario("Manuela Montero", "2005"),
        Usuario("Odón Castillo", "2012"),
        Usuario("Balduino Martín", "1998"),
        Usuario("Leopoldo Vicente", "2017"),
        Usuario("Amaro Hernández", "2019"),
        Usuario("Cristian Bravo", "2009"),
        Usuario("Irene Carrasco", "1999"),
        Usuario("Benedicto Iglesias", "2012"),
        Usuario("Feliciano Jiménez", "2001"),
        Usuario("Calixto Moreno", "2005"),
        Usuario("Alonso Peña", "2020"),
        Usuario("Jordi Cano", "2019"),
        Usuario("Aresio Pascual", "2023"),
        Usuario("Arturo Martín", "2018"),
        Usuario("Damián Lorenzo", "2019"),
        Usuario("Teófanes Vargas", "2015"),    )
}